# Zaber ASCII Arduino Library

Provides implementation for Arduino boards to interact with Zaber devices in the ASCII protocol over serial communications.

## Getting Started Guide

You can get up and running with the following Arduino code to send your device to its home position.

```cpp
#include <ZaberAscii.h>
 
ZaberAscii za(Serial);
 
void setup() {
    /* Initialize baudrate to 115200, typical for Zaber ASCII devices */
    Serial.begin(115200);
 
    /* Issue a home command to device 1 */
    za.send(1, "home");

    /* Always consume the device's reply even if not checking errors. */
    za.receive();
}
 
void loop() {
 
}
```

An additional beginner's guide to getting started is available [here](#) on the Zaber wiki.

## Examples

Arduino `.ino` examples are available in the `examples` directory.

## Documentation

See `ZaberAscii.h`.