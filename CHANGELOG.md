# Changelog

## v1.1.1 (2018-09)

* Fixed an infinite loop bug in pollUntilIdle().
* Removed some superfluous include statements that caused problems under Linux.
* Improved examples to demonstrate consumption of devices' replies to commands; not consuming the
  replies can lead to serial port receive buffer overflow and corruption of subsequent replies.

## v1.1 (2018-05)

* Updated link to user guide.

## v1.0 (2018-03)

* Initial release